defmodule Words do
  @doc """
  Count the number of words in the sentence.

  Words are compared case-insensitively.
  """
  @spec count(String.t()) :: map
  def count(sentence) do
    res = Map.new()
    splitted = String.split(sentence, ~r/[ _,.:"@£$%^&*!]+/, trim: true)
    count_words(splitted, res)
  end

  @spec count_words([String.t()], map) :: map
  def count_words([], map) do
    map
  end

  def count_words([head|tail], map) do
    count_words(tail, Map.update(map, String.downcase(head), 1, &(&1 + 1)) )
  end

end
