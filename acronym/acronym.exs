defmodule Acronym do
  @doc """
  Generate an acronym from a string.
  "This is a string" => "TIAS"
  """
  @spec abbreviate(String.t()) :: String.t()
  def abbreviate(string) do
    string
    |> String.split(~r/[ -]/)                         ## Split by space or -
    |> Enum.map(fn x -> upcase_first(x) end)          ## set the first letter to upcase
    |> List.foldl("", fn x, acc -> acc <> x end)      ## Join the list to a string
    |> to_charlist()
    |> Enum.filter(fn x -> x in (?A..?Z) end)         ## get the upcase letters
    |> to_string()

  end


  defp upcase_first(string) do
    {first, rest} = String.split_at(string, 1)
    String.upcase(first) <> rest
  end
end
