defmodule StringSeries do
  @doc """
  Given a string `s` and a positive integer `size`, return all substrings
  of that size. If `size` is greater than the length of `s`, or less than 1,
  return an empty list.
  """
  @spec slices(s :: String.t(), size :: integer) :: list(String.t())
  def slices(s, size) do
    if size < 1 or size > String.length(s) do
      []
    else
      slices(s, size, [])
    end
  end

  defp slices(s, size, res) do
    if size > String.length(s) do
      res
    else
      {_first, next} = String.next_codepoint(s)
      slices(next, size, res ++ [String.slice(s, 0, size)])
    end
  end
end
