defmodule ProteinTranslation do
  @doc """
  Given an RNA string, return a list of proteins specified by codons, in order.
  """
  @spec of_rna(String.t()) :: {atom, list(String.t())}
  def of_rna(rna) do
    of_rna(rna, [])
  end

  defp of_rna("", res) do
    {:ok, res}
  end

  defp of_rna(rna, res) do
    {status, codon} = of_codon(String.slice(rna, 0, 3))

    cond do
      status == :ok and codon != "STOP" -> of_rna( String.slice(rna, 3, String.length(rna) ), res ++ [codon] )
      status == :error and codon == "invalid codon" -> {:error, "invalid RNA"}
      codon == "STOP" -> {:ok, res}
    end
  end

  @doc """
  Given a codon, return the corresponding protein

  UGU -> Cysteine
  UGC -> Cysteine
  UUA -> Leucine
  UUG -> Leucine
  AUG -> Methionine
  UUU -> Phenylalanine
  UUC -> Phenylalanine
  UCU -> Serine
  UCC -> Serine
  UCA -> Serine
  UCG -> Serine
  UGG -> Tryptophan
  UAU -> Tyrosine
  UAC -> Tyrosine
  UAA -> STOP
  UAG -> STOP
  UGA -> STOP
  """

  @spec of_codon(String.t()) :: {atom, String.t()}
  def of_codon(codon) do
    proteins = %{
      "UGU" => "Cysteine",
      "UGC" => "Cysteine",
      "UUA" => "Leucine",
      "UUG" => "Leucine",
      "AUG" => "Methionine",
      "UUU" => "Phenylalanine",
      "UUC" => "Phenylalanine",
      "UCU" => "Serine",
      "UCC" => "Serine",
      "UCA" => "Serine",
      "UCG" => "Serine",
      "UGG" => "Tryptophan",
      "UAU" => "Tyrosine",
      "UAC" => "Tyrosine",
      "UAA" => "STOP",
      "UAG" => "STOP",
      "UGA" => "STOP",
    }
    result = Map.get(proteins, codon)
    if result == nil do
      {:error, "invalid codon"}
    else
      {:ok, result}
    end

  end
end
