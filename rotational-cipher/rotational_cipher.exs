defmodule RotationalCipher do
  @doc """
  Given a plaintext and amount to shift by, return a rotated string.

  Example:
  iex> RotationalCipher.rotate("Attack at dawn", 13)
  "Nggnpx ng qnja"
  """
  @spec rotate(text :: String.t(), shift :: integer) :: String.t()
  def rotate(text, shift) do
    to_charlist(text)
    |> Enum.map(fn x -> convert(x, shift) end)
    |> to_string()
  end

  defp convert(x, shift) do
    cond do
      (x >= ?a and x <= ?z) -> rem(x - ?a + shift, 26) + ?a
      (x >= ?A and x <= ?Z) -> rem(x - ?A + shift, 26) + ?A
      true          -> x
    end

  end
end

