defmodule Bob do
  def hey(input) do
    cond do
      is_yelling(input) and is_question(input)  -> "Calm down, I know what I'm doing!"
      is_yelling(input)                         -> "Whoa, chill out!"
      is_question(input)                        -> "Sure."
      String.trim(input) == ""                  -> "Fine. Be that way!"
      true                                      -> "Whatever."
    end
  end

  defp is_yelling(string) do
    String.upcase(string) == string and String.upcase(string) != String.downcase(string)
  end

  defp is_question(string) do
    String.ends_with?(string, "?")
  end


end
