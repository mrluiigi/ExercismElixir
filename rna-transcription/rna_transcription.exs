defmodule RNATranscription do
  @doc """
  Transcribes a character list representing DNA nucleotides to RNA

  ## Examples

  iex> RNATranscription.to_rna('ACTG')
  'UGAC'
  """
  @spec to_rna([char]) :: [char]
  def to_rna(dna) do
    to_rna(dna, [])
  end

  @spec to_rna([char], [char]) :: [char]
  def to_rna([], rna) do
    Enum.reverse(rna)
  end

  def to_rna([head|tail], rna) do

    case head do
      ?G -> to_rna(tail, [?C | rna])
      ?C -> to_rna(tail, [?G | rna])
      ?T -> to_rna(tail, [?A | rna])
      ?A -> to_rna(tail, [?U | rna])
    end


  end

end
